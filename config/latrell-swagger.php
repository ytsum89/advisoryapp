<?php
return array(
	'enable' => true,

	'prefix' => 'test-api-documents',

	'paths' => base_path('app'),
	'output' => storage_path('swagger/docs'),
	'exclude' => null,
	'default-base-path' => '/api',
	'default-api-version' => null,
	'default-swagger-version' => null,
	'api-doc-template' => null,
	'suffix' => '.{format}',

	'title' => 'Swagger UI'
);