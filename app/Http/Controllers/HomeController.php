<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Listing;
use App\User;
use Illuminate\Support\Facades\Cache;

class HomeController extends Controller
{
   

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        
        $lists=Listing::paginate(15);
        $users=User::get();
        
        $parameters=array(
            'lists'=>$lists,
            'users'=>$users
        );
        
        return view('home',$parameters);
    }
    
    public function listingEdit(Request $request){
        
        $id=$request->get('id');
        $user_id=$request->get('user_id');

       $listing= Listing::find($id);
       
       if(!$listing){
            return redirect()->back()->with('error','ID not found');
       }
       $listing->fill($request->all());
       $listing->user_id=$user_id;
       $listing->save();
        return redirect()->back()->with('status','Listing has been updated');
    }
    
    public function listingDelete(Request $request){
         $id=$request->get('id');
        $listing= Listing::find($id);

         if(!$listing){
            return redirect()->back()->with('error','ID not found');
       }
              $listing->delete();
                return redirect()->back()->with('status','Listing has been deleted');
    }
    
    public function minionNumber(Request $request){
        
        $value=$request->get('minion-number');
       
        
        $primenumber = Cache::rememberForever('getPrimeNumber', function () {
                return $this->getPrimeNumber();
            });

      
        return redirect()->back()->with('status',substr($primenumber, $value, 5))->withInput();
        
    }
    
    public function getPrimeNumber(){
        $string='';
            $count = 2;  
            $num = 2;  
            
//           if($value <=3798){
//               $num=2;
//           }else if($value <=8568){
//               $num = 7919;  
//           }
            while ($count < 30000 )  
            {  
                if($this->primeCheck($count)){
                    $string.=$count;  
                }
                $count++;
            } 
            
            return $string;
    }
    
    function primeCheck($number){ 
            if ($number == 1) 
            return 0; 
            for ($i = 2; $i <= $number/2; $i++){ 
                if ($number % $i == 0) 
                    return 0; 
            } 
            return 1; 
    } 
}
