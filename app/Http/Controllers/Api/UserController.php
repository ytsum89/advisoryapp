<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Listing;
use App\User;
use App\Classes\JsonResponse;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

/**
 * @SWG\Info(title="My User API", version="0.1")
 */

class UserController extends Controller
{

   
/**
 * user login
 *
 * @SWG\Post(
 *      path="/api/user/login",
 *      operationId="userLogin",
 *      tags={"User"},
 *      summary="user Login",
 *      description="user Login",
*     produces={"application/json"},
 *      @SWG\Parameter(
 *          name="email",
 *          in="formData",
 *          description="user email",
 *          required=true,
 *          type="string",

 *          @SWG\Schema(type="string")
 *      ),
 *      @SWG\Parameter(
 *          name="password",
 *          in="formData",
 *          description="user password",
 *          required=true,
*          type="string",
 *          @SWG\Schema(type="string")
 *      ),
 *      @SWG\Response(
 *          response=200,
 *          description="successful operation"
 *       ),
 *       @SWG\Response(response=400, description="Bad request"),
 *       security={
 *           {"api_key_security_example": {}}
 *       }
 *     )
 *
 */
    
    public function login(Request $request) {

               $jsonResponse = new JsonResponse();
        $credentials = $request->only('email', 'password');


        if (Auth::attempt($credentials)) {
            $currentUser= Auth::user();
           $currentUser->token=str_random(32);
           $currentUser->save();
           
            $result=array(
                'id' => (int)$currentUser->id,
                'token' => $currentUser->token,
            );
            $jsonResponse->setResponse(array($result));
            $jsonResponse->setBody('Access Granted');

            return $jsonResponse->get();
        }else{
             $jsonResponse->setCode(400);
             $jsonResponse->setBody('Wrong Credentials');
             return $jsonResponse->get();
        }
       

        
    }
    
}
