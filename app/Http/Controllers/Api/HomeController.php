<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Listing;
use App\User;
use App\Classes\JsonResponse;
use App\Http\Controllers\Controller;


class HomeController extends Controller
{


    /** @SWG\Get(
     * 		path="/api/listing/list",
     * 		tags={"listing"},
     * 		operationId="list",
     * 		summary="list listing",
     * 		@SWG\Parameter(
     * 			name="token",
     * 			in="query",
     * 			required=true,
     * 			type="string",
     * 			description="token",
     * 		),
     * 		@SWG\Parameter(
     * 			name="id",
     * 			in="query",
     * 			required=true,
     * 			type="string",
     * 			description="user ID",
     * 		),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *       @SWG\Response(response=400, description="Bad request"),
     *       security={
     *           {"api_key_security_example": {}}
     *       }
     * 	)
     *
     */
    public function showList(Request $request) {

        $jsonResponse = new JsonResponse();

        $id = $request->get('id');

        $token = $request->get('token');

        $authUser = User::where('token', $token)->first();

        if (!$authUser) {

            $jsonResponse->setCode(400);
            $jsonResponse->setBody('Invalid Token');
            return $jsonResponse->get();
        }

        $user = User::find($id);
        if (!$user) {
            $jsonResponse->setCode(400);
            $jsonResponse->setBody('User not found');
            return $jsonResponse->get();
        }
        $lists = $user->lists;
        $list_array = array();
        foreach ($lists as $list) {
            $list_array[] = array(
                'id' => (int) $list->id,
                'list_name' => $list->list_name,
                'distance' => $list->distance
            );
        }

        $jsonResponse->setResponse(array('lists' => $list_array));
        $jsonResponse->setBody('Listing successfully retrieved');

        return $jsonResponse->get();
    }

    /**
     * update listing
     *
     * @SWG\Post(
     * 	   path="/api/listing/update",
     *      operationId="updateListing",
     *      tags={"listing"},
     *      summary="update listing",
     *      description="user Login",
     *     produces={"application/json"},
     *      @SWG\Parameter(
     *          name="token",
     *          in="formData",
     *          description="user token",
     *          required=true,
     *          type="string",

     *          @SWG\Schema(type="string")
     *      ),
     *      @SWG\Parameter(
     *          name="listing_id",
     *          in="formData",
     *          description="listing ID",
     *          required=true,
     *          type="string",
     *          @SWG\Schema(type="string")
     *      ),
     *      @SWG\Parameter(
     *          name="list_name",
     *          in="formData",
     *          description="listing name",
     *          required=true,
     *          type="string",
     *          @SWG\Schema(type="string")
     *      ),
     *      @SWG\Parameter(
     *          name="distance",
     *          in="formData",
     *          description="listing distance",
     *          required=true,
     *          type="string",
     *          @SWG\Schema(type="string")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *       @SWG\Response(response=400, description="Bad request"),
     *       security={
     *           {"api_key_security_example": {}}
     *       }
     *     )
     *
     */
    public function listingEdit(Request $request) {

        $jsonResponse = new JsonResponse();

        $id = $request->get('listing_id');

        $token = $request->get('token');

        $authUser = User::where('token', $token)->first();

        if (!$authUser) {

            $jsonResponse->setCode(400);
            $jsonResponse->setBody('Invalid Token');
            return $jsonResponse->get();
        }
        $listing = Listing::find($id);

        if (!$listing) {
            $jsonResponse->setCode(400);
            $jsonResponse->setBody('Listing not found');
            return $jsonResponse->get();
        }
        
        $listing->fill($request->all());
        $listing->save();
        $jsonResponse->setBody('Listing successfully updated');
        return $jsonResponse->get();
    }

    public function listingDelete(Request $request) {
        $id = $request->get('id');
        $listing = Listing::find($id);

        if (!$listing) {
            return redirect()->back()->with('error', 'ID not found');
        }
        $listing->delete();
        return redirect()->back()->with('status', 'Listing has been deleted');
    }

}
