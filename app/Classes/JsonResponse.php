<?php 

namespace App\Classes;
use ArrayObject;

class JsonResponse {

    private $code = 200;
    private $response = '';
    private $body = '';
    private $subject = 'OK';

    public function setCode( $code = 200 )
    {
    	$this->code = $code; 
        
    }

    public function setResponse( $response = [] )
    {
    	$this->response = $response;
    }

    public function setBody( $body = '' )
    {
    	$this->body = $body;
    }

    public function setSubject( $subject = '' )
    {
    	$this->subject = $subject;
    }

    public function get()
    {
        return response()->json([
            'status' => [
                'code' => $this->code,  
                'message' => $this->body
            ],
            'result' => is_array($this->response) ? $this->_nullToEmpty($this->response) : new ArrayObject()
        ], 200); //off for grabee
    }

    protected function _nullToEmpty($obj) {
        $new_json_array = array();
        foreach ($obj as $k => $v) {
            if (is_array($v)) {
                $new_json_array[$k] = $this->_nullToEmpty($v);
                continue;
            }
            if ($v === null) {
                $new_json_array[$k] = '';
                continue;
            }
            $new_json_array[$k] = $v;
        }
        return $new_json_array;
    }
}
