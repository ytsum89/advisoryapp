<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Listing extends Model
{


    protected $table = 'listing';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'list_name', 'distance'
    ];
    
public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
  
}
