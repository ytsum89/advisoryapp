<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    
    protected $table = "listing";

    public function up()
    {
        Schema::create('listing', function (Blueprint $table) {
            $table->increments('id');
            $table->string('list_name',45);
            $table->float('distance');
            $table->unsignedInteger('user_id');

            $table->timestamps();
        });
        
         Schema::table($this->table, function(Blueprint $table)
        {
            $table->index('user_id', 'user_id_foreign');
            $table->foreign('user_id')->references('id')->on('user');
          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
          Schema::table($this->table, function (Blueprint $table) {
       
             if(Schema::hasColumn($this->table, $this->table.'user_id_foreign')){
                      $table->dropForeign('user_id');

            }
         });
         
        Schema::dropIfExists('listing');
    }
}
