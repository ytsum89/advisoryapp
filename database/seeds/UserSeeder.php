<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
      public function run()
    {
         DB::table('user')->insert([
            'email' => 'tester@gmail.com',
            'encrypted_password' => Hash::make('advisoryTester'),
            'type'=>'a'
        ]);
         DB::table('user')->insert([
            'email' => 'user@gmail.com',
            'encrypted_password' => Hash::make('advisoryUser'),
             'type'=>'u'
        ]);
    }
}
