<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::post('/minions/test', 'HomeController@minionNumber')->name('minionNumber');


Auth::routes();

Route::group(['middleware' => ['admin']], function() {
    Route::get('/dashboard', 'HomeController@index')->name('dashboard');
    Route::get('/home', 'HomeController@index')->name('home');

    Route::post('/listing/edit', 'HomeController@listingEdit')->name('listingEdit');
    Route::post('/listing/delete', 'HomeController@listingDelete')->name('listingDelete');

});