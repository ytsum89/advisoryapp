@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if (session('error'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('error') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div><br>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Listing</div>

                <div class="card-body">
                    <table class="table">
                    <thead>
                        <tr>
                          <th scope="col">Name</th>
                          <th scope="col">Distance</th>
                          <th scope="col">User</th>
                          <th scope="col">Handle</th>
                        </tr>
                      </thead>
                       
                        <tbody>
                            @foreach($lists as $list)
                            <tr>
                                <td>{{ $list->list_name }}</td>
                                <td>{{ $list->distance }}</td>
                                <td>{{ $list->user->email }}</td>
                                <td>
                                    <a data-toggle="modal" data-target="#exampleModal" data-info="{{ json_encode($list)}}" href="#">Edit</a> | 
                                    <a data-toggle="modal" data-target="#deleteModal" data-info="{{ json_encode($list)}}" href="#" href="#">Delete</a>
                                </td>
                            </tr>
                            @endForeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">New message</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="{{ route('listingEdit') }}" method="post" id="submit-form">
           @csrf

          <div class="form-group">
            <label for="recipient-name" class="col-form-label">List Name:</label>
            <input type="text" class="form-control" id="list_name" name="list_name" required>
            <input type="hidden" class="form-control" id="list_id" name="id">

          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Distance :</label>
            <input type="number" class="form-control" id="distance" name="distance" required>
          </div>
            <div class="form-group">
                <label for="exampleFormControlSelect1">User select</label>
                <select class="form-control" id="user_id" name="user_id" required>
                    @foreach($users as $user)
                    <option  value="{{ $user->id }}">{{ $user->email }}</option>
                  @endForeach
                  
                </select>
              </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="event.preventDefault();document.getElementById('submit-form').submit();">Update</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">New message</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <p>Confirm to delete this list?</p>
        <form action="{{ route('listingDelete') }}" method="post" id="delete-form">
           @csrf
            <input type="hidden" class="form-control" id="remove_list_id" name="id">
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="event.preventDefault();document.getElementById('delete-form').submit();">Delete</button>
      </div>
    </div>
  </div>
</div>



@endsection

@push('scripts')
<script>
    $(function(){
       $('#exampleModal').on('show.bs.modal', function (event) {
    
            var button = $(event.relatedTarget) // Button that triggered the modal
            var info = button.data('info') // Extract info from data-* attributes
            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            var modal = $(this);
            modal.find('.modal-title').text(info.list_name)
            modal.find('.modal-body #list_name').val(info.list_name);
           modal.find('.modal-body #distance').val(info.distance)
           modal.find('.modal-body #list_id').val(info.id)
           modal.find('.modal-body #user_id').val(info.user_id)

          })
          
          $('#deleteModal').on('show.bs.modal', function (event) {
    
            var button = $(event.relatedTarget) // Button that triggered the modal
            var info = button.data('info') // Extract info from data-* attributes
            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            var modal = $(this);
            modal.find('.modal-title').text(info.list_name)
           modal.find('.modal-body #remove_list_id').val(info.id)

          })
    })

</script>

@endpush