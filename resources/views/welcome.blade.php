<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Advisory App Test</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a  href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}</a>
                                

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                        
                    @else
                        <a href="{{ route('login') }}">Admin Login</a>

                       
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                   Part C Minion Test
                </div>
                
               <form action="{{ route('minionNumber') }}" method="post" id="submit-form">
                    @csrf

                   <div class="form-group">
                     <label for="minion-number" class="col-form-label">Minion Number :</label>
                     <input type="number" class="form-control" id="minion-number" name="minion-number" required min="0" max="10000" value="{{ old('minion-number') }}">
                   </div><br>
                    <button type="submit">Submit</button>
                 </form><br>
                @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            RESULT : {{ session('status') }}
                        </div>
                    @endif
            </div>
        </div>
    </body>
</html>
